# README #

Servidor de Configuração, que tem o objetivo de centralizar toda a configuração da nossa rede de Microservices em um só lugar.

### Informações iniciais ###

* Todos os arquivos de configuração do nosso sistema estarão em um repositório git e o Servidor de Configuração será o responsável por ler as informações no repositório e fornece-las às aplicações através de requests HTTP.
* [Arquitetura de microservices com Spring Cloud e Spring Boot - Parte 2](https://coderef.com.br/arquitetura-de-microservices-com-spring-cloud-e-spring-boot-parte-2-ff03d8d45dba)

### Estrutura de diretórios ###

* Diretório .mvn (Configurações específicas do maven);
* Diretório static (Arquivos estáticos para uso em paginas web);
* Diretório templates (Templates do Spring MVC);
* Arquivo .gitignore (Arquivos ignorados pelo git, teremos um na pasta raiz do projeto delivery);
* Arquivo mvnw (Usar versão e parâmetros expecíficos do maven);
* mvnw.cmd (Usar versão e parâmetros específicos do maven no Windows);

### Dependências ###

* spring-boot-actuator: Se trata de um sub projeto do Spring Boot. Ele adiciona vários serviços de qualidade de produção à sua aplicação com pouco esforço de sua parte como o famoso /health por exemplo.
* spring-boot-starter-web: Essa dependência é uma das mais importantes. Usando-a você já tem um projeto totalmente configurado para trabalhar com qualquer serviço web, como fornecer recursos REST e um tomcat embedded por padrão para subir o projeto.
* spring-cloud-config-server: Aqui está a dependência mágica que irá transformar nosso MicroService em um Servidor de Configuração sem muito esforço adicional, apenas algumas configurações no application.yml.
* spring-boot-starter-test: Por último, e não menos importante, a dependência de testes do Spring. Ela torna nosso projeto apto para a implementação de testes unitários, de API, integração, carga entre diversos outros.

### Arquivos da aplicação ###

* application.yml: Aqui são declaradas as configurações que você gostaria de substituir no starter do Spring boot e serão usadas no seu projeto.
* bootstrap.yml: Esse arquivo é usado para realizar algumas configurações de inicialização do nosso parent, como o nome da aplicação no ecosistema do Spring Cloud e a conexão com um Servidor de Configuração.